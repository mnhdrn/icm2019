import Config


config :icm, IcmWeb.Endpoint,
  server: true,
  http: [port: System.get_env("PORT") || 4000],
  url: [host: System.get_env("HOST") || "localhost", port: System.get_env("PORT") || "4000"],
  cache_static_manifest: "priv/static/cache_manifest.json"
