const home = document.querySelector("#patient_container #home")
const contact = document.querySelector("#patient_container #contact")

const displayHome = () => {
	let home = document.querySelector("#patient_container #home")
	let contact = document.querySelector("#patient_container #contact")

	let classEl = home.classList.contains('active')

	let pageHome= document.querySelector("#patient_session")
	let pageContact= document.querySelector("#patient_contact")

	if (classEl == false) {
		home.classList.add('active')
		pageHome.classList.add('active')
		contact.classList.remove('active')
		pageContact.classList.remove('active')
	}
}

const displayContact = () => {
	let home = document.querySelector("#patient_container #home")
	let contact = document.querySelector("#patient_container #contact")
	let classEl = contact.classList.contains('active')

	let pageHome= document.querySelector("#patient_session")
	let pageContact= document.querySelector("#patient_contact")

	if (classEl == false) {
		home.classList.remove('active')
		pageHome.classList.remove('active')
		contact.classList.add('active')
		pageContact.classList.add('active')
	}
}

if (!!document.querySelector("#patient_container #home"))
	home.addEventListener("click", displayHome);

if (!!document.querySelector("#patient_container #contact"))
	contact.addEventListener("click", displayContact);
