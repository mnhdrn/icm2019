defmodule IcmWeb.Router do
  use IcmWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", IcmWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/patient", IcmWeb do
    pipe_through :browser

    get "/", PatientController, :index

    get "/explain/:id", PatientController, :explain
    get "/exercise/:id", PatientController, :exercise

    get "/pass", PatientController, :pass
    get "/feedback", PatientController, :feedback
  end
end
